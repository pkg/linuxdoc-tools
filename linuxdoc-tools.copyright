
This is `linuxdoc-tools', a series of tools to implement the Linux
Documentation Project HOWTO and book styles in SGML.

--

linuxdoc-tools is derived from linuxdoc-SGML, originally written by
Matt Welsh and later maintained by Cees de Groot. Linuxdoc-SGML is
based on James Clark's sgmls parser, and the QWERTZ DTD by Tom
Gordon. Magnus Alvestad provided the current HTML support. For
the rest of linuxdoc-SGML,

  Copyright (C) 1994-1996 Matt Welsh <mdw@cs.cornell.edu>
  Copyright (C) 1996-1998 Cees de Groot <cg@pobox.com>

Original Linuxdoc-SGML itself does not have any limitations.
Everything not having explicit additional conditions can be freely
used, modified, and redistributed, under the usual disclaimers and
fair use clauses:

 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
   BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
   ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
 * That is, no warranty at all, no liability at all. Use at your own risk.
 * Preserve credits and Copyright notices of the different elements if
   present. Be fair to previous authors.

Since then, lots of smaller and bigger changes resulted in a rename
to SGML-Tools (and then to SGMLtools, the hyphen caused confusion)
to indicate that it wasn't just for Linux anymore. See files
CHANGES.old-v1 and CONTRIBUTORS.old-v1 for changelog and list of
contributors to old linuxdoc-sgml and sgmltools-v1.

When sgml-tools dropped support for the linuxdoc DTD, Taketoshi Sano
<sano@debian.org> forked the code to linuxdoc-tools. See README file.

Changes after the fork

  Copyright (C) 1999-2002 Taketoshi Sano <sano@debian.org>
  Copyright (C) 2000      Juan Jose Amor
  Copyright (C) 2007-2018 Agustin Martin Domingo <agmartin@debian.org>

Unless conflicting with other licenses, changes by Agustin Martin
Domingo are free software: you can redistribute and/or modify them
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version. Otherwise they honour previous
license.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
In Debian systems you can find a copy under /usr/share/common-licenses.

--

See below for more specific Copyright notices for

  sgmls-1.1/LICENSE for sgmlsasp translator license.
  iso-entities/COPYING for iso-entities license.
  entity-map/COPYING for entity-map license.
